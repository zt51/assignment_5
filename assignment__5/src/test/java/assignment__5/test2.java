package assignment__5;

import org.junit.Assert;
import org.junit.Test;

public class test2 {

	@Test
	public void test_readin() {
		KWIC k = new KWIC();
		k.readin();
		int title_size = k.title.size();
		int stop_size = k.stop.size();
		int num1 = 5;
		int num2 = 7;
		Assert.assertEquals(num1, title_size);
		Assert.assertEquals(num2, stop_size);
		k.find_keyword();
		// System.out.print(k.keyword.size());
		int num3 = 10;
		Assert.assertEquals(num3, k.keyword.size());
	}

}
