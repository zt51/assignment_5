package edu.duke.fintech512.terms;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Common {

	public List<String> readin(String from) {
		List<String> read = new ArrayList<>();
		Scanner sc = null;
		String system = "System.in";
		if (!from.equals(system)) {
			try {
				sc = new Scanner(new File(from));
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		} else {
			sc = new Scanner(System.in);
		}
		while (sc.hasNextLine()) {
			read.add(sc.nextLine());
		}
		return read;
	}

}
